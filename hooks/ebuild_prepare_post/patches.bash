# derived from http://kloeriy.livejournal.com/13667.html
#  AND http://dpaste.com/580028/plain/ which was a copy of pippings autopatch changes
# 2013-12-23 AMR TODO: make post AND rebuild build system if necesary

# 2017-03-22 AMR TODO: remove when autohook works for exheres_prepare_post
# 2017-03-22 AMR TODO: profile based patches

source "${PALUDIS_EBUILD_DIR}/die_functions.bash"
source "${PALUDIS_EBUILD_DIR}/echo_functions.bash"
source "${PALUDIS_EBUILD_DIR}/exheres-0/build_functions.bash"

pushd "${S}"
(
	topdir="${ROOT}/etc/paludis/patches"
	patchdirs="
		${topdir}/all/${CATEGORY}
		${topdir}/all/${CATEGORY}/${PN}
		${topdir}/all/${CATEGORY}/${PN}/${PV}
		${topdir}/$(hostname)/${CATEGORY}
		${topdir}/$(hostname)/${CATEGORY}/${PN}
		${topdir}/$(hostname)/${CATEGORY}/${PN}/${PV}
	"
	for d in ${patchdirs} ; do
		if [[ -d "${d}" ]] ; then
			for p in "$d/"* ; do
				if [[ -f "${p}" ]] ; then
					case "${p}" in
						*.bash)
							source "${p}"
						;;
						*.patch)
							expatch -p1 "${p}"
						;;
					esac
				fi
			done
		fi
	done
)
popd
