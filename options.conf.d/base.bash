CONF=$(dirname ${0%/*})

source "${CONF}/machines/$(hostname).conf"

build_options() {
	jobs=$(($(grep processor /proc/cpuinfo | cut -d: -f 2 | sort -n | tail -n1) + 1))

	echo "*/* build_options: jobs=${jobs} symbols=${symbols} dwarf_compress"
}

targets() {
	local packages=(
		app-admin/eclectic-gcc
		dev-util/pkg-config
		sys-devel/binutils
		sys-devel/gcc
	)

	for p in ${packages[@]} ; do
		local out="${p} targets:"
		for t in ${targets[@]} ; do
			out="${out} ${t}"
		done
		echo "${out}"
	done
}

build_options
targets
