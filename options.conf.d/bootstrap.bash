for p in $PALUDIS_BOOTSTRAP ; do
	case "${p}" in
		sys-apps/util-linux)
			echo "sys-apps/util-linux -udev"
		;;
		sys-devel/gcc)
			echo "sys-devel/gcc -threads"
		;;
		*)
			echo "${p} bootstrap"
		;;
	esac
done
