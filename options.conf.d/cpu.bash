my_features=""

cpus=(
	x86
	amd64
	arm
)

x86_features=(
	3dnow
	3dnowext
	avx
	avx2
	fma3
	fma4
	mmx
	mmxext
	sse
	sse2
	sse3
	ssse3
	sse4.1
	sse4.2
	sse4a
)

amd64_features=(
	3dnow
	3dnowext
	avx
	avx2
	fma3
	fma4
	sse3
	ssse3
	sse4.1
	sse4.2
	sse4a
)

arm_features=(
	iwmmxt
	neon
)

for cpu in ${cpus[@]} ; do
	my_features="${my_features}\n*/* ${cpu}_cpu_features: "
	curr_cpu="${cpu}_features[@]"

	for feature in ${!curr_cpu} ; do
		if grep flags /proc/cpuinfo | head -n1 | cut -d: -f2 | sed 's, ,\n,g' | sed 's,_,.,g' | tail -n+2 | grep -Eo "^${feature}">/dev/null ; then	
			[[ "${feature}" == "ssse3" ]] && my_features="${my_features} sse3"
			my_features="${my_features} ${feature}"
		fi
	done
done

echo -e "${my_features}"
