CONF=$(dirname ${0%/*})

parse_mask() {
	[[ -f "${CONF}/tests/${1}.conf" ]] || return -1
	while read line ; do
		if [[ "${line:0:1}" == "#" ]] ; then
			echo "${line}"
		elif [[ "${line}" ]] ; then
			echo "${line} ${2}"
		fi
	done < "${CONF}/tests/${1}.conf"
}

source "${CONF}/machines/$(hostname).conf"

echo "*/* build_options: $(for t in ${tests[@]} ; do echo -n "${t}_tests " ; done)"

echo "${tests[@]}" | grep recommended >/dev/null && cat << EOF
dev-lang/python sqlite
EOF

echo "${tests[@]}" | grep expensive >/dev/null && cat << EOF
# sys-libs/db[tcl] required for expensive tests
# sys-libs/db tcl
EOF

parse_mask mask "build_options: -recommended_tests -expensive_tests"
parse_mask expensive "build_options: -expensive_tests"

opts=`set +o`
set -f

for p in ${PALUDIS_TRACE_PKGS} ; do
	echo "${p} build_options: trace"
done

for p in ${PALUDIS_SKIP_TESTS} ; do
	echo "${p} build_options: -recommended_tests -expensive_tests"
done

`${opts}`
