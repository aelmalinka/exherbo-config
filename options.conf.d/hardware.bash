CONF=$(dirname ${0%/*})

source "${CONF}/machines/$(hostname).conf"

opts=`set +o`
set -f

vidstring="*/* video_drivers:"
for v in ${video_cards[@]} ; do
	case $v in
		radeon)
			#echo "x11-dri/mesa llvm video_drivers: gallium-swrast"
		;;
	esac

	vidstring="${vidstring} ${v}"
done

echo $vidstring

`${opts}`
