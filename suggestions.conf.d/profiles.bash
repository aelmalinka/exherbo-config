CONF=$(dirname ${0%/*})
DIR=$(basename -s .conf.d ${0%/*})

source "${CONF}/machines/$(hostname).conf"

for profile in ${profiles[@]} ; do
	if [[ -d "${CONF}/profiles/${DIR}" ]] ; then
		if [[ -f "${CONF}/profiles/${DIR}/${profile}.conf" ]] ; then
			cat "${CONF}/profiles/${DIR}/${profile}.conf"
		fi
	fi
done
