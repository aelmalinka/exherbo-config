CONF=$(dirname ${0%/*})
ME=$(basename -s .bash ${0})

source "${CONF}/machines/$(hostname).conf"
source "${CONF}/bashrc"

# 2017-04-01 AMR NOTE: linking to each target
if [[ "${ME}" == "installed" ]] ; then
	echo "format = exndbam"
	echo "location = \${root}/var/db/paludis/installed"
	echo "split_debug_location = /usr/${CHOST}/lib/debug"
	echo "tool_prefix = ${CHOST}-"
else
	echo "format = exndbam"
	echo "location = \${root}/var/db/paludis/cross-installed/${ME}"
	echo "name = ${ME}"
	echo "split_debug_location = /usr/${ME}/lib/debug"
	echo "tool_prefix = ${ME}-"
	echo "cross_compile_host = ${ME}"
fi
