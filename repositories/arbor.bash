CONF=$(dirname ${0%/*})

source "${CONF}/machines/$(hostname).conf"

location="\${root}/var/db/paludis/repositories/arbor"

function cpuname() {
	machine="$1"
	[[ -z "$machine" ]] && machine="$(uname -m)"
	case "${machine}" in
		x86_64)
			echo "amd64"
		;;
		*)
			echo "$machine"
		;;
	esac
}

function add_profile() {
	prof="${prof} \${location}/profiles/${1}"
}

add_profile "$(cpuname)"

# 2021-11-29 AMR TODO: pretty sure profiles in arbor aren't actually used
#for profile in ${profiles[@]} ; do
#	test -d "\${location}/profiles/${profile}" && add_profile "${profile}"
#done

cat << EOF
location = ${location}
sync = git+https://git.exherbo.org/arbor.git local: git+file:///usr/src/Exherbo/arbor
format = e
profiles = ${prof}
EOF
