source /etc/paludis/machines/$(hostname).conf

CHOST="${host}"

x86_64_pc_linux_gnu_CFLAGS="${flags[@]}"
x86_64_pc_linux_gnu_CXXFLAGS="${flags[@]}"
x86_64_pc_linux_musl_CFLAGS="${flags[@]}"
x86_64_pc_linux_musl_CXXFLAGS="${flags[@]}"
i686_pc_linux_gnu_CFLAGS="${flags[@]}"
i686_pc_linux_gnu_CXXFLAGS="${flags[@]}"
i686_pc_linux_musl_CFLAGS="${flags[@]}"
i686_pc_linux_musl_CXXFLAGS="${flags[@]}"

replace-flag() {
	export x86_64_pc_linux_gnu_CFLAGS="$(echo "${x86_64_pc_linux_gnu_CFLAGS}" | sed "s,$1,$2,g")" 
	export x86_64_pc_linux_gnu_CXXFLAGS="$(echo "${x86_64_pc_linux_gnu_CXXFLAGS}" | sed "s,$1,$2,g")"
	export x86_64_pc_linux_musl_CFLAGS="$(echo "${x86_64_pc_linux_musl_CFLAGS}" | sed "s,$1,$2,g")" 
	export x86_64_pc_linux_musl_CXXFLAGS="$(echo "${x86_64_pc_linux_musl_CXXFLAGS}" | sed "s,$1,$2,g")"
	export i686_pc_linux_gnu_CFLAGS="$(echo "${i686_pc_linux_gnu_CFLAGS}" | sed "s,$1,$2,g")"
	export i686_pc_linux_gnu_CXXFLAGS="$(echo "${i686_pc_linux_gnu_CXXFLAGS}" | sed "s,$1,$2,g")"
	export i686_pc_linux_musl_CFLAGS="$(echo "${i686_pc_linux_musl_CFLAGS}" | sed "s,$1,$2,g")" 
	export i686_pc_linux_musl_CXXFLAGS="$(echo "${i686_pc_linux_musl_CXXFLAGS}" | sed "s,$1,$2,g")"
}

filter-flags() {
	local flag=

	for flag in $@ ; do
		replace-flag "${flag}" ""
	done
}

case "${CATEGORY}/${PN}" in
	net-www/chromium-*)
		filter-flags -fvar-tracking-assignments
		#replace-flag -g3 -g2
	;;
	sys-devel/gcc)
		filter-flags -Wall -Wextra -pedantic -Werror
	;;
esac
